### Analytic pipeline development

#### Motivation and design goals

Our lab pioneered the transition of micropatterning from low throughput to high throughput platforms
[@zandstra_nazareth_2013; @zandstra_tewary_2019],
which we have employed to study the spatial organization of cell fate at unprecedented throughput
[@zandstra_tewary_2017; @zandstra_tewary_2019; @zandstra_ostblom_2019].
In general,
high-throughput platforms bring several benefits over lower-throughput assays,
mainly centered around faster query of experimental conditions
and increasing number of replicates,
which enable more thorough searches of system parameter spaces
and higher reproducibility of results.

To be able to analyze this wealth of data in a statistically sound and systematic manner,
we developed software tailored for this purpose.
First a package to work together with commonly used image processing pipeline for extraction of single cell data,
and here without the need to rely on single cell features,
and instead work directly with the images in a high or low throughput setting.
The design goals of this platform,
was to build a semi-automated approach to quantify cell fate organization in colonies of cells,
and to create this software framework in an environment that is both easy to use for scientist without significant computational training
while allowing great flexibility and power for experienced users.

As we wanted to make this platform extendible and available
to as many scientists as possible without restrictions,
we have implemented all parts of the framework using open source software
and released it under an open license.
We believe it is paramount for scientists to rely on open tools
to the greatest possible extent as these increase transparency,
reproducibility, and accessibility to research.
While many hurdles persists for the larger research community
to transition from obsolete models of closed software and article publishing,
every software and publication that is made openly available
is a small step towards advancing science in a direction
which leads to time-savings and increased resource availability for researchers.

#### Plot and widget interactivity

To meet our design goals,
we implemented our framework as a set of graphical widgets in an open programmable environment.
Specifically,
we used Jupyter Notebooks [@granger_perez_2007; @willing_kluyver_2016] via JupyterLab,
as they allow for display of rich content inline with code in the same browser-based interface.
This allowed us to leverage web technologies such as HTML and Javascript
to create interactive widgets via Panel [@ballester_rudiger_2020] and Param [@corona_stevens_2020],
and interactive plots using Bokeh [@bokehdevelopmentteam_bokehdevelopmentteam_2020] and Holoviews [@kbowen_rudiger_2020].
Interactivity is a central component of our analytical framework
as widgets makes it widely accessible to a large number of scientists
and speeds up parameter optimization
as users can see changes represented in the microscopy images in real time
when they drag sliders or in other ways update the widgets.
The inclusion of interactive plots provides rich information on individual data points
and through the display of images for selected data points,
this allows scientist to directly relate relate measurements back to microscopy images,
combining the strengths of extracted quantitative image metrics
with the pattern recognition capabilities of the human visual system.
Importantly,
this also contributes to the ease of use of the platform
since for every data points it is easy to view the image of the cell colony,
which simplifies the intuitive understanding of how metrics are derived
and reduces the risk of introducing unseen errors.

Notably,
while interactivity and rendering of thousands of data points
can make performance suffer
Bokeh allows easy use of client side GPU-accelerated rendering via WebGL
to improve performance and smoothly render hundreds of thousands of interactive data points.
Our choice of software ecosystem allows this to be extended to hundreds of millions points when needed
via Datashader [@ahmadia_bednar_2020] which seamlessly re-bins pixel data
to push high-level, dynamically updated image representations to the browser instead of all individual data points,
an approach we're currently using to display large high quality images of stitched wells without slowing down browser performance.
Although the main focus of this framework is interactive data visualization,
we recognize the need to incorporate state data representation into traditional publishing formats,
which is here achieved through the use of an alternative rendering engine
to optionally export plots as scalable vector graphics (.svg files)
as they are rendered in the browser.

As Jupyter Notebooks are rendered by browser engines
without required specialized software on the client machine,
this make the transition to a separate server and client machine seamless.
A Jupyter kernel can be started on a powerful workstation,
and when set up via JupyterHub or SSH,
users can access it via a web address on the network.
This offers not only a major increase in performance,
since computations are carried out on the server,
but avoids error prone copying of data to personal laptops,
and frees up time to use workstations in person,
while still allowing client side interactivity and easy access to saved files.
<!--TODO tqdm? -->

#### Framework architecture

Our analytical framework relies on several well-established scientific Python packages
to perform the quantification of image data.
Specifically,
we use pandas [@wesmckinney_wesmckinney_2010; @mortadamehyar_reback_2020]
as the backbone for storing and operating on data using dataframes,
scikit-image [@yu_vanderwalt_2014] and numpy [@varoquaux_vanderwalt_2011] for image processing,
statsmodels [@perktold_seabold_2010], scipy[@others_jones_2001], scikit-learn [@duchesnay_pedregosa_2011],
and scikits-bootstrap for deriving statistical quantities of data,
joblib [@joblibdevelopmentteam_joblibdevelopmentteam_2020] and pandarallel to increase performance through parallel computing,
and numba [@seibert_lam_2015] to further speed up computations
by compiling custom Python functions at runtime.
While we're currently relying on storing the image data in memory to maximize performance,
the architecture allows for easy enhancement using external memory algorithms as implemented in Dask and Ray possible via Modin.
This would allow large imaging dataset to be processed even on personal laptops,
albeit at the cost of a performance decrease.


#### Analysis workflow

There are three main steps involves in the analysis process,
first colonies of cells are identified in microscopy images,
which requires the use of a nuclear dye to stain all cells in the colony
(future versions aim to also allow identification via use brightfield images).
Identified colonies can be filtered based on their
area, circularity, solidity (area ratio to their bounding box),
and proximity to the image border (**[@Fig:col-id]**).
To identify suitable thresholds for these parameters,
we use scatter plots similar to how subpopulations are identified using gating in flow cytometry (**[@Fig:qc-plot]**).
Since it is not immediately obvious where a certain threshold should lie just from the data points in the plot,
selection of data points displays the corresponding microscopy images
to facilitate the decision whether these data points should be included or excluded from the analysis (**[@Fig:qc-plot]B-F**).

\clearpage

![**Parameter optimization for colony identification.**
Graphical interface for the colony identification widget
showing the code run in the input cell at the top,
the displayed widget with sliders used to adjust the colony filters.
and the image plot where identified colonies are rendered in magenta, cyan, and yellow.
Three different colors were chosen to give increased dissolving power over one color for neighboring aggregates,
but avoiding the visual noise from using unique colors for each colony.
](fig/ch3/analytic-framework/colony-identification.pdf){#fig:col-id short-caption='Parameter optimization for colony identification' width=88%}

\clearpage

![**Quality control plot for identified colonies.**
**A)** Plotting the circularity versus area (also encoded as marker size) of each colony allows interactive exploration of different set of colonies by selecting the plotted markers.
Marker color indicates density where dense regions are colored yellow and sparse regions pruple.
Here, several different selections made on the same plot are shown in the different panels,
where the unselected colonies are made increasinly transparent to empasize the selection. 
For each of the selected colonies,
an image is displayed for that colony's nucelar stain.
The panels show a selection of
**B**) very large colonies
**C**) large colonies with low circularity
**D**) colonies of around twice the area and lower circularity than the densest population, similar to flow cytometry, these are "duplets" where colonies have fused together,
**E**) small, non-circular colonies,
**F**) circular single colonies, the population of interest.
](fig/ch3/analytic-framework/qc-plots.pdf){#fig:qc-plot short-caption='Quality control plot for identified colonies' width=77%}

\clearpage

\newpage

The next step is to set a threshold for which pixels are classified as positive and which are negative.
<!--TODO this should really be a span... and the ons inside are excluded as neither pos or neg -->
This is optional and enables the calculation of additional metrics
such as proportion of pixel phenotypes per colony
which is a more meaningful metric than an average intensity value of arbitrary units
and easier to compare between screens.
The thresholding widget allows for the display of individual colonies
cropped out of the original images
where multiple colonies might be in the same image.
A number of parameters such as minimum threshold area, smoothing image preprocessing,
and the maximum number of separate threshold regions can be specified (**[@Fig:thresholding]A**).

While the threshold value can be set manually using visual inspection
or stained positive and negative controls,
there is also an automated threshold estimation,
which samples a configurable number of pixels from all colonies (500,000 by default)
and uses the first derivative of the kernel density estimate of the pixel intensity distribution
to find suitable areas for setting a threshold.
The reason for using the kernel density estimate
is to provide a smooth, gradually increasing and decreasing curve
without local variation leading to the detection of false peaks and valleys.
Using the rate of change of a smoothened curve is commonly used to identify
minima and maxima,
i.e. locations where the first derivative changes sign,
however,
in our experience working with immunofluorescence imaging data,
fluorophore intensities rarely form curves with clear minima close to the ideal thresholding range 
(where thresholds would have been put by manual inspection of images).
Rather,
positive pixel values stretch over a wide range,
which has the effect that intensity curves often gradually flatten out
as the negative pixel distribution transitions into the positive.
In our experience,
the ideal threshold value is close to this flattening
after the first intensity peak
which we here refer to as a "flatima"
and corresponds to where the first derivative of the intensity profile approaches,
but don't necessary reach,
zero.
<!--TODO ref include movie -->
Setting a fixed distance for the tolerance
of how close to zero the first derivative need to reach in order to define a flatima,
is not feasible,
since the variation of the first derivative can differ greatly between distributions (**[@Fig:thresholding]B**).
Instead,
we define the tolerance interval as relative to the standard deviation of first derivative,
more precisely as

$$tol = (1 + sd)^{1/5} - 1$$

where $sd$ refers to the standard deviation of the first derivative of the intensity profile.
In addition,
we disregard flatimas within one standard deviation of the peak from the negative pixel distribution
to avoid that the threshold is put on regions soon after a maxima
where the rate of change is also low.

As should be clear from the presentation by now,
these are empirically derived definitions,
but they allow us to consistently define thresholds between experiments and plates,
instead of risking the inclusion of errors from variation in human performance
when setting thresholds.
The exponent in the tolerance formula and the maximas' proximity distance
can be tweaked to suitable values depending on
the protein expression profiles, fluorophore intensities, and laser configuration.
It should be noted that we tried several well establish image thresholding and clustering methods before developing our own approach,
including Otsu thresholding, Li (entropy) based thresholding, Yen thresholding, Gaussian mixture models, Bayesian Gaussian mixture models, and K-means clustering.
None of these performed as well for our particular purpose as the method we developed.

In addition to using the intensity threshold for defining pixel phenotypes,
it will also impact the location of the center of mass each phenotype region.
This is because only positive pixels are considered when calculating the center of mass
to reduce the effect of background staining
which would otherwise pull all centers artificially closer to the colony centroid (**[@Fig:thresholding]C**).

\clearpage

![**Semi-automatic intensity thresholding**
**A)** The graphical user interface of the thresholding widget.
**B**) A kernel density estimate for a pixel intensity distribution
(blue curve and underlying histogram)
with high (top) and low (bottom) variation
in the first dierivate of the profile (orange curve).
The grey horizontal band denotes the tolerance level around zero,
and the dotted grey line is where the threshold is set,
which is where the first derivative is inside the tolerance level
(the orange curve touches the grey band).
The left y-axis show the count of pixels,
the right y-axis show the change in the first derivative,
and the x-axis show the normalized intensity values.
**C)** A colony intensity image with (left) and without (right) background staining.
In both images,
the magenta-colored dot represents the center of mass for only the positive pixels
(calculated from the intensity image to the right)
whereas the cyan dot represents the center of mass when background staining is included
(calculated from the intensity image to the left).
](fig/ch3/analytic-framework/threshold-identification.pdf){#fig:thresholding short-caption='Semi-automatic intensity thresholding' width=100%}

\newpage

After defining appropriate thresholds,
several colony metrics are calculated,
including shape properties such as area, aspect ratio (elongation), solidity, and circularity
which allows us to stratify colonies by shape in assays where this is of interest.
We plan to add calculation of these characteristics also for the positive pixel areas in the future.
For the pixel intensities,
we calculate the center of mass for positive pixel regions,
and use subtract background staining from images so that these can be overlaid together in heatmaps of average intensity expression for multiple colonies in the same condition (as in **[@Fig:2iL-vs-LS]D**).
The distance between the center of mass for different markers
can then be used to determine its offset from the colony centroid,
which is a measure of the degree of asymmetrical expression in the colony.

To interactively work with the data and all these metrics,
we use the same widget and plotting strategy as after the colony identification (**[@Fig:viz-cols]A**).
Importantly,
multiple plotting panels for different measures can be shown simultaneously
and by selecting cells in one plotting panel,
they will be highlighted in the other panels as well,
showing how a population of cells score for several different metrics in the same view.
If a categorical variable (such as treatment condition) is used to color the plots,
trend lines can be added using scatterplot smoothing (LOWESS)
that help distinguish the overall pattern between conditions.
When not comparing two continuous variable,
the plotting interface allows for up to three levels of nested categorical variables on the x-axis,
which facilitates comparisons between multiple variables over creating separate plots for different categories and the trying to compare between plots.
To create the most representative visual semantics
and allow for the most accurate comparison of colonies between conditions,
we developed a variation of scatter plots where points are laid out
according to the kernel density distribution of all points in the same group.
The shape of this distribution is analogous to a violin plot,
but does not have the drawbacks of masking the number of observations,
and making small distributions appear unnaturally smooth.
In comparison to a swarm/hive plot,
our modification has the advantages of avoiding issues with binning based on marker size
(similar to a KDE curve versus a histogram),
not having to deal with saturation once the plot grows too wide,
performing better for big datasets,
and being based on a commonly used statistical property.
The minimal appearance of this plot with just the points
allows for the easy addition of population statistics as graphical annotation
without making the plot too busy.
The framework currently allows the addition of a measure of central tendency such as the mean or median,
together with a 95% confidence interval using bootstrapping with replacement.
95 % confidence intervals are convenient tools
to approximate the chance by which observations might have appeared by chance
as non-overlapping intervals indicate a p-value of around 0.01
and those overlapping by 25% of their total length indicate a p-value around 0.05.
This rough indication of statistical significance
also promotes a sound mental model
which deemphasize the reliance of reaching exactly p<0.05.
<!--TODO add to figure -->
<!--TODO insert video of working with the panel here -->

![**Visualization of colony metrics.**
Graphical interface for the visualization of colony metrics.
Several x and y variables can be plotted simultaneously in multiple plots,
and plot asthetics can also be controlled from the widget.
](fig/ch3/analytic-framework/visualize-colony-metrics.pdf){#fig:viz-cols short-caption='Visualization of colony metrics' width=100%}

